class PokerHand {
    RoyalFlush = (array) => {
        const royalArray = ['j', 'q', 'k', 'a', 10];
        const isSuits = array.every((card, _ , array) => array[0].suit === card.suit);
        const isRanks = royalArray.every((rank) => array.some((card)=> card.rank === rank));

        if (isSuits && isRanks){
            return true;
        }

        return false;
    }

    StraightFlush = (array) => {
        const isSuits = array.every((card, _ , array) => array[0].suit === card.suit);
        const isRanks = array
            .sort((a, b) => a.rank - b.rank)
            .every((card, index, array ) => {
                if (index < 4){
                    return array[index + 1].rank - card.rank === 1;
                }

                return array[array.length-1].rank - array[array.length - 2].rank === 1;
            });

        if (isSuits && isRanks){
            return true;
        }

        return false;
    }

    Flush = (array) => {
        const isSuits = array.every((card, _ , array) => array[0].suit === card.suit);

        if (isSuits){
            return true;
        }
        return false;
    }

    Pare = (array) => {
        let pare = 0;
        for (let i = 0; i < array.length; i++) {
            for (let j = i + 1; j < array.length; j++) {
                if (array[i].rank === array[j].rank) {
                    pare ++;
                }
            }
        }

        return pare;
    }


    Kind = (array) => {
        const results = {};

        array.forEach(card => {
            if (results.hasOwnProperty(card.rank)) {
                results[card.rank]++
            } else (
                results[card.rank] = 1
            )
        })

        for(let key in results) {
            if (results[key] === 3) {
                return 3;
            }

            if (results[key] === 4) {
                return 4;
            }
        }

        return  '';
    }

    FullHouse = (array) => {
        const results = {};

        array.forEach(card => {
            if (results.hasOwnProperty(card.rank)) {
                results[card.rank]++
            } else (
                results[card.rank] = 1
            )
        })

        if (Object.keys(results).length === 2) {
            return true
        }

        return  false;
    }


    results(array) {
        if (this.RoyalFlush(array)){
            return 'Royal Flush'
        }

        if (this.StraightFlush(array)){
            return 'Straight Flush'
        }

        if (this.Flush(array)) {
            return 'Flush'
        }

        if (this.FullHouse(array)) {
            return 'Full House'
        }

        if (this.Kind(array) === 3){
            return  'Three of Kind'
        }

        if (this.Kind(array) === 4){
            return  'Four of Kind'
        }

        if (this.Pare(array) === 2) {
            return 'two pairs'
        }

        if(this.Pare(array) === 1) {
            return 'one pair'
        }
    };
}
export default PokerHand;