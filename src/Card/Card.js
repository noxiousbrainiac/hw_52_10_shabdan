import React from "react";
import './Cards.css'

const suits = {
    '♠': 'spades',
    '♥': 'hearts' ,
    '♦': 'diams',
    '♣': 'clubs'
}

class Card extends React.Component {
    render() {
        return (
            <div className={"card " + this.props.back + this.props.rank + " " + suits[this.props.suit]}>
                <span className="rank">{this.props.rank}</span>
                <span className="suit">{this.props.suit}</span>
            </div>
        )
    }
}

export default Card;