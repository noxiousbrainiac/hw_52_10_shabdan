class CardDeck{
    constructor() {
        this.ranks = ['j', 'q', 'k', 'a', 2, 3, 4, 5, 6, 7, 8, 9, 10];
        this.suits = ['♦', '♥', '♠', '♣'];
        this.cardDeck = [];

        this.ranks.map(rank => {
            this.suits.map(suit => {
                this.cardDeck.push({
                    suit,
                    rank
                })
            })
        })
    }

    getCard(){
        const random = Math.floor(Math.random() * this.cardDeck.length)
        const card = this.cardDeck[random];

        this.cardDeck.splice(random, 1);
        return card;
    }

    getCards(howMany) {
        const cards = [];

        for (let i = 0; i < howMany; i++){
            cards.push(this.getCard());
        }
        return cards;
    }
}

export default CardDeck;
