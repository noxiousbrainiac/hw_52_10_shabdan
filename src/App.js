import './App.css';
import {Component} from "react";
import Card from "./Card/Card";
import CardDeck from "./Card/CardDeck";
import PokerHand from "./Card/PokerHand";


class App extends Component {
    state = {
        fiveCards: [
            {back: 'back', rank: '', suit: ''},
            {back: 'back', rank: '', suit: ''},
            {back: 'back', rank: '', suit: ''},
            {back: 'back', rank: '', suit: ''},
            {back: 'back', rank: '', suit: ''}
        ],
        result: ''
    }

    getDeck = () => {
        const cardDeck = new CardDeck();
        const cards = cardDeck.getCards(5);

        this.setState({
            fiveCards: [
                {back:'rank-', rank: cards[0].rank, suit: cards[0].suit},
                {back:'rank-', rank: cards[1].rank, suit: cards[1].suit},
                {back:'rank-', rank: cards[2].rank, suit: cards[2].suit},
                {back:'rank-', rank: cards[3].rank, suit: cards[3].suit},
                {back:'rank-', rank: cards[4].rank, suit: cards[4].suit}
            ]
        }, () => this.getOutcome())
    }

    getOutcome = () => {
        const hand = new PokerHand();
        const results = hand.results(this.state.fiveCards);

        this.setState({
            result: results
        },);
    };

    render() {
        return (
            <div className="container">
                {this.state.fiveCards.map((card, index) => {
                    return (
                        <Card
                            key={index}
                            back={card.back}
                            suit={card.suit}
                            rank={card.rank}
                        />
                    )
                })}
                <div className="body">
                    <button className="btn" onClick={this.getDeck}>Run</button>
                    <div className="result">
                        <h2>{this.state.result}</h2>
                    </div>
                </div>
            </div>
        )
    }
}

export default App;
